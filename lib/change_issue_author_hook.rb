module ChangeIssueAuthorHook
  class Hooks < Redmine::Hook::ViewListener
    render_on :view_issues_form_details_top, :partial => 'issues/change_issue_author'
    render_on :view_issues_bulk_edit_details_bottom, :partial => 'issues/bulk_edit_issue_author'

=begin
    def helper_issues_show_detail_after_setting(context = {})
      detail = context[:detail]

      case detail.property
      when 'attr'
        if detail.prop_key == 'author_id'
          detail[:value] = User.find detail.value 
          detail[:old_value] = User.find detail.old_value
        end
      end
    end
=end
  end
end
